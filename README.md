Personal repository of Chloe Rouyer.

# Welcome

Repository containing a subset of my projects that can be made publicly available.
Many of my other projects have been made during courses that I either took during
my master degree, or that I am teaching. As a result, those files should not be
made available to respect the integrity of the course.
Below you can find a short description of some pieces of code that I present here.


## Online learning
In Online learning, we can empirically test the performance of algorithms in the
stochastic regime. 

### Adaptation to the effective range of the losses in prediction with limited advice (two-point feedback)

Code orignally used for my master thesis.
In this framework, we test algorithms presented in my master thesis to see how their
regret scales (in terms of the number of arms K, the effective range of the losses
epsilon, and the suboptimality gaps Delta).
We test out the approach of decoupling exploration and exploitation to select the
secondary arm B_t, compared to the currently used uniform exploration and exploitation.

### Offline evaluation of Bandit algorithms.

Code requires the file ydata-fp-td-clicks-v2_0.20111002 in the R6_B dataset from the
Yahoo Webscope program:
R6B - Yahoo! Front Page Today Module Us
Dataset: ydata-fp-td-clicks-v2_0
Yahoo! Front Page Today Module User Click Log Dataset, version 2.0

If we don't want to generate stochastic data to test our model, real life evaluation
of online learning algorithms is generally quite difficult. One approach consists in
using offline evaluation. In this dataset, the arms are selected uniformly from a set
of arms, so both important weighted sampling and rejection sampling are valid methods
in this setting.
We test out the performance of different algorithms.


## Image analysis
Here, we tackle different problems of image analysis such as feature tracking, image
classification using Convolutional Neural Networks, and deriving affine structure from
motion.
A description of the 3 assignments is presented in the README in the image_analysis folder.


## Reinforcement learning
Here are some algorithms from a reinforcement learning course that I can share. In the
Q-learning and the CMA-ES folders, the majority of the code was given in advance.

### MDP
We consider Markov Decision Processes, and we code an algorithm to derive the value of
a policy and derive the optimal policy.

### Q-Learning
An introduction exercise to the use of tensorflow for Q-learning.

### CMA-ES
An introduction exercise on the use of Covariance Matrix Adaptation Evolution Strategy.
