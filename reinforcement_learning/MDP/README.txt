Assignment 2 ATML
Chloe Rouyer

Exercise written in MATLAB.

Given the grid world in gridworld-empty.png, you have to navigate through the doors in order to reach the Terminal State. Each movement has a cost of 1, Some rooms have an additional cost factor when you enter them, and bumping in a wall leaves you in the same room, while still suffering the cost of the movement.
The goal of the exercise is to compute the value of the uniformly random policy, and then to use a reinforcement learning algorithm to compute the optimal value function and the optimal policy.


To reproduce the results, run gridworldscript.m

Note:
The values in V are treated as one vector in the algorithms, but is reshaped into the grid world shape to get a better view of the results. Similarly, the actions are not encoded as ["up", "down", "left", "right"] in the algorithms, but displayed as such to improve readibilit y of the results.

