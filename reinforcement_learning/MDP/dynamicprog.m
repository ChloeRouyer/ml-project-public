function [Vk1, i] = dynamicprog(theta, V0, A, P, R, pi, S, gamma)
% Implementation of Dynamic programming the first algorithm from the
% Reinforcement learning lecture slides.
% The algorithm denotes Vk as the current state and Vk1 as the new one.
% This implementations assumes deterministic P and R 

Delta = 2* theta;
i = 0;

Vk = V0;
Vk1 = V0;

while Delta >= theta
    Delta = 0;
    
    for s = S
        v = Vk1(s);
        Vk1(s) = 0;
        for a = A
            ss = P(a, s);
            ad = pi(s, a)* (R(a, s) + gamma*Vk(ss));
            Vk1(s) = Vk1(s) + ad;
        end
        Delta = max(Delta, abs(v - Vk1(s)));
    end
    i = i+1;
    Vk = Vk1;
end

end

