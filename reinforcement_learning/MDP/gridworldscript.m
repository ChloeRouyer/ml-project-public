% Grid world exercise 
% Written by Chloe Rouyer

% % Parameters used for the problem:

theta = eps; %% Threshold
gamma = 1; %% Discount factor = 1 because finite MDP

% a = {up, down, right, left}
a = 1:4;

% States S are of the shape. (1 is the top left corner, 3 the top right ...
% and 12 the bottom right final state

S = 1:12;

% all rewards and transitions are deterministic so we only save the
% mappings: given action a and state s, which is the new state?

P = zeros([4, 12]); % transition prob P(a, s) = s'
                      % that action a in state s results in state s'
  
P(a, 1) = [1, 4, 2, 1];
P(a, 2) = [2, 5, 3, 1]; 
P(a, 3) = [3, 6, 3, 2];

P(a, 4) = [1, 7, 4, 4];
P(a, 5) = [2, 8, 5, 5];
P(a, 6) = [3, 9, 6, 6];

P(a, 7) = [4, 10, 8, 7];
P(a, 8) = [5, 11, 9, 7];
P(a, 9) = [6, 9, 9, 8];

P(a, 10) = [7, 10, 10, 10];
P(a, 11) = [8, 11, 12, 11];
P(a, 12) = [12, 12, 12, 12];


% all reward are -1 except entering the rooms that have a negative cost and
% the terminal state
R = ones([4, 12]) * -1;

R(2, 2) = -6;
R(1, 8) = -6;

R(2, 4) = -11;
R(1, 10) = -11;
R(4, 8) = -11;

R(2, 6) = -6;
R(3, 8) = -6;

% R(3, 11) = 0;
R([1, 2, 3, 4], 12) = 0;

% random policy
randompi = @(s, a) 1/4* ones(size(a));

V0 = zeros(size(S));

"V of the Random policy"
[Vrand, i] = dynamicprog(theta, V0, a, P, R, randompi, S, gamma);
V_random_policy = reshape(Vrand, [3, 4])'



"Optimal V and pi"
[Vopt, piopt, it] = valueiteration(theta, V0, a, P, R, S, gamma);
V_optimal = reshape(Vopt, [3, 4])'

actions = ["up", "down", "right", "left"];
pi_optimal = reshape(actions(piopt), [3, 4])'

