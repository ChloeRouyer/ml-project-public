function [Vk, pi, it] = valueiteration(theta, V, a, P, R, S, gamma)
% Implementation of Dynamc programming the third algorithm from the
% Reinforcement learning lecture slides.
% The algorithm denotes Vk as the current state and Vk1 as the new one.
% This implementations assumes deterministic P and R 

Delta = 2* theta;
pi = zeros(size(S));
it = 0;

Va = ones(size(a));
Vk = V;
Vk1 = V;
while Delta >= theta
    Delta = 0;
    
    for s = S
        v = Vk(s);
        Va = R(a, s) + gamma*Vk(P(a, s))';
        [Vk1(s), pi(s)] = max(Va');
      
        Delta = max(Delta, abs(v - Vk1(s)));
    end
    it = it+1;
    Vk = Vk1;
    pi;
end
    
end

